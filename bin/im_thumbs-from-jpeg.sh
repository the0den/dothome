#!/bin/sh
mkdir thumbs thumbs_low
mogrify -path thumbs -define jpeg:size=1280x960 -auto-orient -thumbnail '640x480>' -strip '*.jpg'
mogrify -path thumbs_low -define jpeg:size=1280x960 -auto-orient -thumbnail '640x480>' -strip -quality 50 -sampling-factor 2x1 '*.jpg'
#mogrify -path thumbs_png -format PNG8 -define jpeg:size=1280x960 -auto-orient -thumbnail '640x480>' -strip -quality 05 '*.jpg'
