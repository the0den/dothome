# aliases for django 1.3x manage.py
alias dj='python manage.py'
alias djr='python manage.py runserver'
alias djrl='python manage.py runserver 8000'
alias djrw='python manage.py runserver --werkzeug'
alias djsm='python manage.py schemamigration'
alias djm='python manage.py migrate'
alias djdb='python manage.py syncdb'
alias djs='python manage.py shell'
alias djmm='python manage.py makemessages -l ru -e .html -e .htm -e .txt'
alias djcm='python manage.py compilemessages'
alias djv='python manage.py validate'

# aliases for git
alias g='git'
alias gcl='git clone'
alias ga='git add .'
alias gc='git commit -am'
alias gco='git checkout'
alias gs='git status'
alias gps='git push'
alias gpl='git pull'
alias gsh='git stash'
alias gsha='git stash apply'
alias gd='git diff'
alias gl='git log -3 --stat'
alias gb='git branch -vv'

# bash
alias jl='jobs -l'
# as suggested at https://askubuntu.com/questions/17275/how-to-show-the-transfer-progress-and-speed-when-copying-files-with-cp/17380#comment777083_201250s
alias cp="rsync -ah --progress"